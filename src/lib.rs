
#[macro_use]
pub mod geo2d;
pub mod phys2d;

pub use crate::geo2d as geo;

pub use crate::phys2d as phys;

pub use geo::Circle as Circle;
pub use geo::Point as Point;
pub use geo::Vect as Vect;

pub use phys::Mobile as Mobile;


