    
use super::geo2d::{Circle, Vect,};

    pub struct Mobile {
        pos : Circle,
        speed : Vect,
        orientation : f32,
    }

    impl Mobile {
        pub fn collide(&self, other: &Mobile) -> bool {
            self.pos.collide(&other.pos)
        }

        pub fn new(pos: Circle, speed : Vect) -> Mobile{
            Mobile {
                pos: pos,
                speed: speed,
                orientation : 0_f32,
            }
        }

        pub fn apply_speed(&mut self) {
            self.pos.translate(&self.speed);
        }

        pub fn position(&self) -> &Circle {
            &self.pos
        }

        pub fn speed(&self) -> &Vect {
            &self.speed
        }

        pub fn accelerate(&mut self, accel: Vect) {
            self.speed = self.speed + accel;
        }

        pub fn rotate(&mut self, angle:f32) {
            self.orientation += angle;
            while self.orientation > 360_f32 {
                self.orientation -= 360_f32
            }
            while self.orientation < 0_f32 {
                self.orientation += 360_f32
            }
        }
    }


#[cfg(test)]
mod libtest {
    use super::Mobile;
   
    #[test]
    pub fn test_collision(){
        let m1 = Mobile::new(circle!(1.,2.,2.), vect!(0.,0.));
        let m2 = Mobile::new(circle!(1.,5.,2.), vect!(0.,0.));
        assert!(m1.collide(&m2));
        assert!(m2.collide(&m1));
    }

    #[test]
    pub fn move_mobile(){
        let mut m1 = Mobile::new(circle!(-1.,1.,2.), vect!(1.,-1.));
        let m2 = Mobile::new(circle!(0.,0.,2.), vect!(1.,-1.));
        m1.apply_speed();
        assert_eq!(m1.position(), m2.position());
    }

    #[test]
    pub fn accel_mobile(){
        let pos = circle!(0.,0.,1.);
        let speed = vect!(2.,2.);
        let mut mobile = Mobile::new(pos, speed);
        mobile.accelerate(vect!(1.,1.));
        assert_eq!(mobile.speed(), &vect!(3.,3.));

        mobile.apply_speed();
        assert_eq!(mobile.position(), &circle!(3.,3.,1.));


    }
}