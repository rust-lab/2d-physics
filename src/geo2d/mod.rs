
#[derive(Debug, Clone, Copy)]
pub struct Point  {
    x: f32,
    y: f32,
}

#[derive(Debug, Clone, Copy)]
pub struct Vect {
    x: f32,
    y: f32,
}

#[derive(Debug)]
pub struct Circle {
    center: Point,
    radius: f32,
}

// Traits implementations

// EQ
pub const EPSILON:f32 = 0.0001f32;

impl  PartialEq for Point{
    fn eq(&self, other:&Self) -> bool{
        (self.x-other.x)<EPSILON && (self.y-other.y)<EPSILON
    }
} 
impl PartialEq for Vect{
    fn eq(&self, other:&Self) -> bool{
        (self.x-other.x)<EPSILON 
        && (self.y-other.y)<EPSILON
    }
} 
impl  PartialEq for Circle{
    fn eq(&self, other:&Self) -> bool{
        (self.center == other.center)  
        && (self.radius-other.radius)<EPSILON
    }
}

// Add, Sub..
/// Add impl. consumes the point
impl std::ops::Add<&Vect> for Point  {
    type Output = Point;

    /// consumes the point
    fn add(self, _rhs: &Vect) -> Self::Output {
            Self::Output {
                x:self.x+_rhs.x, 
                y:self.y+_rhs.y}
    }
}
impl std::ops::Add for Vect{
    type Output = Vect;
    fn add (self, _rhs: Vect) -> Self::Output {
        Self::Output {
                x:self.x+_rhs.x, 
                y:self.y+_rhs.y}
    }
}
impl std::ops::Neg for Vect{
    type Output = Vect;
    /// Value is moved
    fn neg (self) -> Self::Output {
        
        Self::Output {
                x: -self.x, 
                y: -self.y}
    }
}

impl std::ops::Mul<f32> for Vect{
    type Output = Vect;

    fn mul(self, _rhs: f32) -> Self::Output {
        Self::Output {
            x: _rhs*self.x,
            y: _rhs*self.y
        }
    }
}

// Methods
impl Point {
    pub fn new(x: f32, y:f32) -> Point {
        Point {x, y}
    }

    pub fn square_dist(&self, other: &Point) -> f32{
        (self.x-other.x)*(self.x-other.x) + (self.y-other.y)*(self.y-other.y)
    }

    pub fn dist(&self, other: &Point) -> f32 {
        self.square_dist(other).sqrt()
    }

    /// Translates a point by vector
    /// 
    /// Consumes the point
    /// #Example
    /// ```
    ///     use physics::geo2d::{Point, Vect};
    ///     let p = Point::new(1., 2.);
    ///     let v = Vect::new(0., 5.);
    ///     let p = p.translate(&v);
    /// ```
    pub fn translate(self, v: &Vect ) -> Point {
        Point { x: self.x + v.x,
                y: self.y + v.y}
    }
}

impl Vect {
    /// Creates a 2Dvector
    pub fn new (x:f32, y:f32) -> Vect {
        Vect {x, y}
    }

    /// Creates 2D vector from 2 points : p1->p2
    pub fn from_points(p1:&Point, p2: &Point) -> Vect {
        Vect {
            x: p2.x-p1.x,
            y: p2.y-p1.y
        }
    }

    /// Compute square norm of the vect
    /// 
    pub fn square_norm(&self) -> f32 {
        self.x.powi(2)+self.y.powi(2)
    }

    /// Compute norm of the vect
    pub fn norm(&self) -> f32 {
        self.square_norm().sqrt()
    }

    /// Normalize the vector
    /// Returns Option::None for (0,0) vector
    /// ```
    ///     use physics::geo2d::Vect;
    ///     let v = Vect::new(2.5, 5.);
    ///     let v = v.normalize().expect("v is null !");
    /// ```
    pub fn normalize(self) -> Option<Vect> {
        let norm = self.norm();
        if norm < EPSILON {
            None
        } else {
            Some(Vect{x:self.x/norm, y:self.y/norm})
        }
    }

    /// Multiply by scalar
    pub fn mult(self, scalar: f32) -> Vect {
        Vect{ x: scalar*self.x, 
        y: scalar*self.y}
    }

    /// Rotate the vector
    pub fn rotate(self, a: f32) -> Vect {
        Vect {
            x: self.x*a.cos() - self.y*a.sin(),
            y: self.x*a.sin() + self.y*a.cos(),
        }
    }

    pub fn scalar(self, other: Vect) -> f32 {
        self.x*other.x + self.y*other.y
    }

    pub fn angle(self, other:Vect) -> Option<f32> {
        let u = self.normalize();
        let v = other.normalize();

        match u {
            Some(u1) => match v {
                Some (v1) => Some((u1.scalar(v1).acos())),
                None => None
            },
            None => None
        }
    }
}

impl Circle {
    pub fn new(center : Point, radius: f32) -> Circle {
        Circle{
            center,
            radius,
        }
    }

    pub fn get_center(&self) -> &Point {
        &self.center
    }

    /// moves the circle by a vector. borrows
    pub fn translate(&mut self, v:&Vect) {
            self.center = self.center.translate(v); 
    }

    pub fn collide(&self, other:&Circle) -> bool {
        self.center.square_dist(&other.center) 
            - (self.radius + other.radius).powi(2) 
            < EPSILON
    }
}


#[macro_export]
macro_rules! point {
    ($x:expr, $y:expr) => {
        crate::geo::Point::new($x, $y)
    };
}

#[macro_export]
macro_rules! vect {
    ($x:expr, $y:expr) => {
        crate::geo::Vect::new($x, $y)
    };
}

#[macro_export]
macro_rules! circle {
    ($c:expr, $r:expr) => {
        crate::geo::Circle::new($c, $r)
    };
    ($x:expr, $y:expr, $r:expr) => {
        crate::geo::Circle::new(crate::geo::Point::new($x, $y), $r)
    };
}

//////////////////
///   TESTS    ///
//////////////////

#[cfg(test)]
mod libtest {

    use super::{Point, Vect, EPSILON};
    
    #[test]
    fn equality() {
        let p1 = Point::new(1., 2.);
        let p2 = point!(1.,2.);

        assert_eq!(p1, p2);
        assert_eq!(p2, p1);

        let p3 = point!(1.,1.);
        assert_ne!(p1, p3);
        assert_ne!(p2, p3);
    }

    
    #[test]
    fn square_dist() {
        let base = Point::new(1f32,1f32);
        assert_eq!(base.square_dist(&Point::new(1.,3.)), 4.);
        assert_eq!(base.square_dist(&Point::new(4.,1.)), 9.);
        let second = Point::new(3.,4.);
        assert_eq!(base.square_dist(&second), 13.);
        assert_eq!(second.square_dist(&base), 13.);
        assert_eq!(Point::new(1.,-1.).square_dist(&Point::new(1.,3.)), 16.);
    }

    #[test]
    fn new_point(){
        let a = &Point::new(2.,6.);
        let b = &Point::new(3.,5.);
        let ab = vect!(1.,-1.);
        let ba = vect!(-1.,1.);

        assert_eq!(Vect::from_points(a, b), ab);
        assert_eq!(Vect::from_points(b, a), ba);

        let _c = point!(1.2, 3.5);
    }

    #[test]
    fn translate(){
        let base = Point::new(1_f32,1_f32);
        let vector = Vect::new(2_f32,3_f32);
        assert_eq!(base.translate(&vector), Point::new(3.,4.));
        assert_eq!(base+&vector, Point::new(3.,4.));
        assert_eq!(base.translate(&vector), Point::new(3.,4.));
    }

    #[test]
    fn add_vect(){
        let v1 = Vect::new(2.5f32, 3f32);
        let v2 = Vect::new(-2f32, 3.5f32);
        let res = Vect::new(0.5f32, 6.5f32);
        assert_eq!(v1+v2, res);

    }

    #[test]
    fn mul_vect(){
        let v1 = Vect::new(2.5f32, 3f32);
        let res = Vect::new(-5f32, -6f32);
        assert_eq!(v1*-2., res);
        assert_eq!((v1*-2.).norm(), 2.*v1.norm());

        // Test distribution
        let v2 = vect!(3.2, -7.);
        let a = 5.2;
        let r1 = v1*a+ v2*a;
        let r2 = (v1+v2)*a;

       // println!("r1.y - r2.y < f32.epsilon ? {}, espilon = {}", r1.y - r2.y < std::f32::EPSILON, std::f32::EPSILON);

        assert_eq!(r1, r2);
    }

    #[test]
    fn neg_mul_vect(){
        let v1 = Vect::new(2.5f32, 3f32);
        let res = Vect::new(-2.5f32, -3f32);
        assert_eq!(-v1, res);
        assert_eq!(v1*-1., res);
        assert_eq!(v1*-2., res*2.);
        assert_eq!((v1*3.).norm(), 3.*v1.norm());
    }

    #[test]
    fn sq_norm_vect(){
        let v1 = Vect::new(-5f32, 3f32);
        let res = 34f32;
        assert_eq!((v1).square_norm(), res);
        assert_eq!((-v1).square_norm(), res);
        assert_eq!(v1.rotate(3.6*core::f32::consts::PI).square_norm(), res);
    }

    #[test]
    fn norm_vect(){
        let v1 = Vect::new(-4f32, 3f32);
        let res = 5f32;
        assert_eq!((v1).norm(), res);
        assert_eq!((-v1).norm(), res);
        assert_eq!(v1.rotate(12f32).norm(), res);
    }

    #[test]
    fn rotate_vect(){
        
        let v1 = Vect::new(-5f32, 3f32);
        let res = -v1;
        assert_eq!(v1.rotate(std::f32::consts::PI), res);
        let rot90 = Vect::new(-3., 5.);
        assert_eq!(v1.rotate(std::f32::consts::PI/2.), rot90);
        let rot270 = Vect::new(3., 5.);
        assert_eq!(v1.rotate(-std::f32::consts::PI/2.), rot270);
    }

    #[test]
    fn move_circle(){
        let mut c1 = circle!(point!(2.,3.), 3.);
        c1.translate(&vect!(2.,4.));
        assert_eq!(c1, circle!(point!(4.,7.), 3.));
    }

    #[test]
    fn collisions(){
        let c1 = circle!(1.,1.,2.);
        let mut c2 = circle!(1.,2.,2.);
        assert!(c1.collide(&c2));
        c2.translate(&vect!(0.,3.01));
        assert!(!c1.collide(&c2));
    }

    #[test]
    pub fn normalize(){
        let v = Vect::new(2.5, 5.);
        let v = v.normalize().expect("oopsie");
        assert!(v.norm()-1. < EPSILON);

        let v = vect!(0.,0.);
        assert!(v.normalize().is_none());
    }
}
